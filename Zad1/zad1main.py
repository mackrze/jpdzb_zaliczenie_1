import math
import sys

if __name__ == '__main__':

    if len(sys.argv) != 5:  # czy jest poprawna liczba arg
        sys.stdout.write('Please enter proper amount of arguments \n')
        sys.exit(1)
    try:  # konwertowanie na int
        x1 = int(sys.argv[1])
        y1 = int(sys.argv[2])
        x2 = int(sys.argv[3])
        y2 = int(sys.argv[4])
        print('Input OK: Starting program')
    except ValueError:
        sys.stdout.write('Please enter a proper number')
        sys.exit(1)

    a: float = (y1 - y2) / (x1 - x2)
    b: float = y1 - (a * x1)
    print('a = {}, b = {}'.format(a, b))

