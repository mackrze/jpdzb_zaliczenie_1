import sys
import os

if __name__ == '__main__':
    if len(sys.argv) != 3:  # czy jest poprawna liczba arg
        sys.stdout.write('Please enter proper amount of arguments \n')
        sys.exit(1)
    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text = os.path.join(dir_path, sys.argv[1])  # for problem with finding file by name
        f = open(text)
    except FileNotFoundError:
        sys.stdout.write('File not exist \n')
        sys.exit(1)
    try:
        n = int(sys.argv[2])
        if n < 0:
            sys.stdout.write('Second argument must be greater than zero \n ')
    except ValueError:
        sys.stdout.write('Second argument must be number \n')
        sys.exit(1)

    words_in_text = []
    for line in f:
        for word in line.split():
            words_in_text.append(word)
    f.close()
    # print(len(words_in_text))
    if len(words_in_text) == 0:  # jesli plik jest pusty
        sys.stdout.write('File is empty \n')
        sys.exit(1)
    if n > len(words_in_text):  # jesli za malo wyrazow
        sys.stdout.write('Number of words in file is smaller than n, error \n')
        sys.exit(1)
    for i in range(0, n):
        print(words_in_text[i])
