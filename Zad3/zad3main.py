import math


class Macierz:

    def __init__(self, a11, a12, a21, a22):
        """ constructor of vector with given vx and vy arguments"""
        self.a11 = a11
        self.a12 = a12
        self.a21 = a21
        self.a22 = a22
        if not isinstance((a11, a12, a21, a22), (int, float)):
            return

    def __repr__(self):
        return {"a11 = ": self.a11, "a12 = ": self.a12, "a21 = ": self.a21, "a22 = ": self.a22}

    def wyznacznik(self):
        return self.a11 * self.a22 - self.a21 * self.a12

    def mnozenie(self, skalar):
        if not isinstance(skalar, (int, float)):
            return
        return Macierz(self.a11 * skalar, self.a12 * skalar, self.a21 * skalar, self.a22 * skalar)


if __name__ == '__main__':
    macierz = Macierz(3, 4, 5, "zle")  # obsluga zlego arg
    macierz = Macierz(3, 4, 5, 6)
    print(macierz.__repr__())  # reprezentacja macierzy
    print(macierz.wyznacznik())  # wyznacznik
    macierz2 = macierz.mnozenie("zle")
    macierz2 = macierz.mnozenie(10)  # mnozenie przez skalar
    print(macierz2.__repr__())
